package intercept

import (
	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/headers"
	"bitbucket.org/viewDelta/service-core/models"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"github.com/dennisstritzke/httpheader"
)

var (
	authGatway = newInterceptor()
)
// SecurityFilter will check whether request is allowed or not for the user
func SecurityFilter(echoContext echo.Context) error{
	var reqToken headers.TokenHeader
	log.Info("Entering SecurityFilter.SecurityFilter() function")
	//Header validation --> Token [SignIn Verify Token]
	if err := httpheader.Bind(echoContext.Request().Header, &reqToken); err != nil {
		log.Errorf("Header parsing error [%v]", err)
		return fmt.Errorf(constants.INVALID_HEADER)
	}
	_, err := authGatway.AccessCheck(reqToken.Token, echoContext.Request().URL.Path,
								echoContext.Request().Method)
	log.Info("Exiting SecurityFilter.SecurityFilter() function")
	return err
}

// GetUserFromToken will return the user details from TOKEN
func GetUserFromToken(echoContext echo.Context) (*models.Users){
	user,err:=authGatway.GetUserFromToken(echoContext.Request().Header.Get(constants.TOKEN_HEADER))
	if err!=nil{
		log.Errorf("error fetching token-user details - %v",err)
		return nil
	}
	return user
}