package intercept

import (
	"fmt"

	"bitbucket.org/viewDelta/service-core/config"
	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/headers"
	"bitbucket.org/viewDelta/service-core/models"
	"bitbucket.org/viewDelta/service-core/repository"
	"bitbucket.org/viewDelta/service-core/repository/users"
	"github.com/casbin/casbin/v2"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/mongo"
)

//Interceptor is the abstract which authorization can be checked
type Interceptor struct {
	casbinEnforcer *casbin.Enforcer
	userDAO        repository.UserDAO
}

var newIntercept *Interceptor

//New returns new interceptor object to call for access check
func newInterceptor() *Interceptor {
	if newIntercept == nil {
		ce, err := config.GetCasbinEnforcer()
		if err != nil {
			log.Errorf("RBAC creation or load error [%v]", err)
			return nil
		}
		newIntercept = &Interceptor{
			userDAO:        users.New(),
			casbinEnforcer: ce,
		}
	}
	return newIntercept
}

//AccessCheck will validate the user access to endpoints
func (itr *Interceptor) AccessCheck(token, Endpoint, HTTPMethod string) (*models.Users, error) {
	user,err:=itr.GetUserFromToken(token)
	if err!=nil{
		return nil,err
	}
	//RBAC Validation
	if !itr.isAuthorized(user.Role, Endpoint, HTTPMethod) {
		log.Errorf("User [%v] Not Authorised For the selected action to perform", user.Email)
		return nil, fmt.Errorf(constants.UNAUTHORISED_ACCESS)
	}
	return user, nil
}

//IsAuthorized will check for the following request type is valid for the user defined roles
func (itr *Interceptor) isAuthorized(roles []string, Endpoint, HTTPMethod string) bool {
	for _, v := range roles {
		temp, err := itr.casbinEnforcer.Enforce(v, Endpoint, HTTPMethod)
		if err != nil {
			log.Errorf("Casbin Enforce Check Error [%v]", err)
		} else if temp == true { //Checking for entire role range
			return true
		}
	}
	return false
}

func (itr *Interceptor) GetUserFromToken(token string) (*models.Users, error){
	var reqHeaders headers.TokenHeader
	reqHeaders.Token = token
	reqHeaders.SetSession()
	user, err := reqHeaders.ValidateToken(itr.userDAO)
	if err != nil {
		log.Errorf("Request Process Error = [%v]", err)
		//Checking if record doesn't exists
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf(constants.DATA_NOT_FOUND)
		}
		return nil, err
	}
	return user,nil
}