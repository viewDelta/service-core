package intercept

import (
	"fmt"

	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/models"
	"bitbucket.org/viewDelta/service-core/repository"
	"bitbucket.org/viewDelta/service-core/repository/users"
	"bitbucket.org/viewDelta/service-core/utils"
	"github.com/labstack/gommon/log"
)

var (
	userDAO repository.UserDAO
)

func init() {
	userDAO = users.New()
}

//NonAdminValidator will check that given uid is non-admin one
func NonAdminValidator(uid string) (*models.Users, error) {
	if uid == "" {
		return nil, fmt.Errorf(constants.INVALID_ENDPOINT)
	}
	user, err := userDAO.FindUserByID(uid)
	if err != nil {
		log.Errorf("UserID - [%v] Fetch Error [%v]", uid, err)
		return nil, err
	}
	if utils.Contains(user.Role, constants.AdminID) {
		log.Errorf("Admin Account Can't be changed")
		return nil, fmt.Errorf(constants.INVALID_OPERATION)
	}
	return user, nil
}

