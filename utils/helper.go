package utils

import (
	"bitbucket.org/viewDelta/service-core/constants"
	"encoding/json"
	"fmt"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/mongo"
	"regexp"
	"strings"
)

//Contains will check whether entry is present w.r.t to content
// Returns true, if contains else returns false
func Contains(slice []string, item string) bool {
	set := make(map[string]struct{}, len(slice))
	for _, s := range slice {
		set[s] = struct{}{}
	}
	_, ok := set[item]
	return ok
}

//IsBlankString will check, if given string is blank or not
func IsBlankString(in string) bool {
	return len(strings.TrimSpace(in)) == 0
}

//IsBlankSlice will check, if given slice is empty or not
func IsBlankSlice(in []string) bool {
	return len(in) == 0
}

// UseObjectMapper to map source and target via json tag
// pass target as address reference in order this to work
func UseObjectMapper(source interface{}, target interface{}) error {
	if jsonData, err := json.Marshal(source); err != nil {
		return err
	} else if err := json.Unmarshal([]byte(jsonData), target); err != nil {
		return err
	}
	return nil
}

// ConvertValidationErrToString will map the validator error key-value msg to only value type 
func ConvertValidationErrToString(err error) error{
	return fmt.Errorf(after(err.Error(),"Error:"))
}

func after(value string, a string) string {
    // Get substring after a string.
    pos := strings.LastIndex(value, a)
    if pos == -1 {
        return ""
    }
    adjustedPos := pos + len(a)
    if adjustedPos >= len(value) {
        return ""
    }
    return value[adjustedPos:len(value)]
}

// CastDBError will convert framework db error to our application required message
func CastDBError(err error) error{
	if err != nil {
		if err == mongo.ErrNoDocuments {
			log.Errorf("No Record was found")
			return fmt.Errorf(constants.DATA_NOT_FOUND)
		}
		log.Errorf("Error occured while retriving - %v", err)
		return fmt.Errorf(constants.APPLICATION_ERROR)
	}
	return nil
}

// check if compiles with given regex
func IsValidWithRegex(regex, input string) bool{
	return regexp.MustCompile(regex).MatchString(input)
}