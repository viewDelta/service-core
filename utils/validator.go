package utils

import (
	"fmt"
	"regexp"
	"strings"
	
	"bitbucket.org/viewDelta/service-core/repository/roles"

	"bitbucket.org/viewDelta/service-core/cache"
	"bitbucket.org/viewDelta/service-core/cache/storeroles"
	"bitbucket.org/viewDelta/service-core/config"
	"bitbucket.org/viewDelta/service-core/models"

	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/repository"
	"bitbucket.org/viewDelta/service-core/repository/users"
	"github.com/labstack/gommon/log"

	countrycodes "github.com/launchdarkly/go-country-codes"
)

//Ivalidator will instance for validation object
type Ivalidator interface {
	ValidatePwd(password string) error
	ValidateEmailExsits(Email string) (*models.Users, bool)
	ValidateRoleID(roleIDs []string) error
	ValidateName(name string) bool
}

type validator struct {
	userDAO    repository.UserDAO
	roleCache  cache.RolesCache
	emailRegex string
	nameRegex  string
}

var ref *validator

//NewValidatorInstance will return validator instance if we are accessing only non-role based fetch operation,
//otherwise it will throw nil reference panic
//if roleCacheRequired = false is set, then it should be only used for email and password validation only
func NewValidatorInstance(roleCacheRequired bool) (Ivalidator, error) {
	if ref == nil {
		userDAO := users.New()
		if roleCacheRequired {
			roleCache, err := storeroles.New()
			if err != nil {
				log.Errorf("Role Cache Initalized error [%v]", err)
				return nil, err
			}
			ref = &validator{
				userDAO:    userDAO,
				roleCache:  roleCache,
				emailRegex: config.GetConfiguration().Validation.EmailRegEx,
				nameRegex:  config.GetConfiguration().Validation.NameRegEx,
			}
			return ref, nil
		}
		ref = &validator{
			userDAO:    userDAO,
			emailRegex: config.GetConfiguration().Validation.EmailRegEx,
			nameRegex:  config.GetConfiguration().Validation.NameRegEx,
		}
	}
	return ref, nil
}

//ValidatePwd will check for password validation
func (ref *validator) ValidatePwd(password string) error {
	//Password Validation
	if len(strings.TrimSpace(password)) >= 6 {
		return nil
	}
	return fmt.Errorf(constants.INVALID_PASSWORD)
}

// ValidateEmailExsits will validate whether provided EMAIL satisfies both regex & database validation
func (ref *validator) ValidateEmailExsits(Email string) (*models.Users, bool) {
	if ref.emailFormatValidation(Email) {
		user, err := ref.userDAO.FindUser(Email)
		if err != nil {
			log.Errorf("Record Fetch Error [%v]", err)
			return nil, false
		}
		//Returning true excluding db collisions
		return user, true
	}
	return nil, false
}

func (ref *validator) emailFormatValidation(email string) bool {
	re := regexp.MustCompile(ref.emailRegex)
	return re.MatchString(email)
}

//ValidateRoleID will validate role id is present or not
func (ref *validator) ValidateRoleID(roleIDs []string) error {
	var (
		allRoles []models.Roles
		err      error
	)
	if ref.roleCache != nil {
		allRoles, err = ref.roleCache.GetAll()
	} else {
		allRoles, err = roles.New().GetAll()
	}
	if err != nil {
		log.Errorf("Record Fetch Error - [%v]", err)
		if allRoles == nil {
			return fmt.Errorf(constants.RECORD_FETCH_ERROR)
		}
	}
	if roleIDs == nil || len(roleIDs) == 0 {
		return fmt.Errorf(constants.ROLEID_REQUIRED)
	}
	for _, v := range roleIDs {
		if !containsRoleID(allRoles, v) {
			log.Errorf("RoleID Not Found For Input - %v", v)
			return fmt.Errorf(constants.ROLEID_NOT_EXISTS)
		}
	}
	return nil
}

//containsRoleID will check if roleID contains in the slice or not
//returns true, if entry exists otherwise false
func containsRoleID(allRoles []models.Roles, roleID string) bool {
	flag := false
	for _, v := range allRoles {
		if v.RoleID == roleID {
			flag = true
		}
	}
	return flag
}

// ValidateName will check with respective regEx
func (ref *validator) ValidateName(name string) bool {
	re := regexp.MustCompile(ref.nameRegex)
	return re.MatchString(name)
}

// IsValidAlpha2CountryCode will validate 2 digit countryCode 
func IsValidAlpha2CountryCode(countryCode string) error{
	if _, ok := countrycodes.GetByAlpha2(countryCode); !ok {
		return fmt.Errorf(constants.INVALID_COUNTRY_CODE+" - %v", countryCode)
	}
	return nil
}