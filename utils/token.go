package utils

import (
	"fmt"
	"time"

	"bitbucket.org/viewDelta/service-core/config"
	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/models"
	"github.com/dgrijalva/jwt-go"
)

// Create the JWT key used to create the signature
var jwtKey = []byte(config.GetEnv(constants.JwtKeyEnv))

//Claims => We add jwt.StandardClaims as an embedded type, to provide fields like expiry time
type Claims struct {
	Email, Password, FirstName, LastName string
	Role                                 []string
	jwt.StandardClaims
}

//Token => Token Details
type Token struct {
	TokenID      string
	ReqAt, ExpAt time.Time
}

//GetToken will create token user details
func GetToken(user *models.Users, client string) (*Token, error) {
	if client != constants.CLIENT {
		return nil, fmt.Errorf(constants.INVALID_CLIENT_CODE)
	}
	var tokenDetails Token
	//token exp time declaration
	expirationTime := time.Now().AddDate(0, 1, 0)
	tokenDetails.ReqAt = time.Now()
	tokenDetails.ExpAt = expirationTime
	// Create the JWT claims, which includes the username and expiry time
	claims := &Claims{
		Email:     user.Email,
		Password:  user.Password, //encpryted password
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Role:      user.Role,
		StandardClaims: jwt.StandardClaims{
			IssuedAt: time.Now().Unix(),
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
			Issuer:    client,
		},
	}
	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return nil, err
	}
	tokenDetails.TokenID = tokenString
	return &tokenDetails, nil
}

//GetTokenDetails will return the token contents in struct and errors if any
func GetTokenDetails(token string) (*Claims, error) {
	claims := &Claims{}
	if _, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	}); err != nil {
		return nil, err
	}
	return claims, nil
}

//RenewToken will return renewed token with all information as before token
func RenewToken(tokenID string) (*Token, error) {
	var tokenDetails Token
	claims, err := GetTokenDetails(tokenID)
	if err != nil {
		return nil, err
	}
	tokenDetails.ReqAt = time.Now()
	tokenDetails.ExpAt = time.Now().AddDate(0, 1, 0)
	claims.IssuedAt = tokenDetails.ReqAt.Unix()
	claims.ExpiresAt = tokenDetails.ExpAt.Unix()
	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return nil, err
	}
	tokenDetails.TokenID = tokenString
	return &tokenDetails, nil
}
