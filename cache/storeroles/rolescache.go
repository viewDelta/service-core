package storeroles

import (
	"encoding/json"

	"bitbucket.org/viewDelta/service-core/cache"
	"bitbucket.org/viewDelta/service-core/config"
	"bitbucket.org/viewDelta/service-core/models"
	"bitbucket.org/viewDelta/service-core/repository"
	"bitbucket.org/viewDelta/service-core/repository/roles"
	"github.com/go-redis/redis"
	"github.com/labstack/gommon/log"
)

type roleCache struct {
	redisClient *redis.Client
	roleRepo    repository.RoleDAO
}

var storeRoles *roleCache

//New will return new storeRoles reference object
func New() (cache.RolesCache, error) {
	if storeRoles == nil {
		//Getting redis config object
		client, err := config.GetRedisClient()
		if err != nil {
			return nil, err
		}
		storeRoles = &roleCache{
			redisClient: client,
			roleRepo:    roles.New(),
		}
	}
	return storeRoles, nil
}

func (roleCache *roleCache) GetAll() ([]models.Roles, error) {
	log.Infof("Looking In Redis for - [Key - %v]", cache.AllRoleKey)
	if val, err := roleCache.redisClient.Get(cache.AllRoleKey).Result(); err != nil {
		log.Debugf("Unable to find property in Redis - [Key - %v] [Error - %v]", cache.AllRoleKey, err)
		//Fetching from database
		if allRoles, err := roleCache.roleRepo.GetAll(); err != nil {
			return nil, err
		} else {
			log.Infof("Setting value in redis for - [Key - %v]", cache.AllRoleKey)
			byteData, err := json.Marshal(allRoles)
			if err != nil {
				log.Errorf("Json Marshall Error - [%v]", err)
				return nil, err
			}
			if err = roleCache.redisClient.Set(cache.AllRoleKey, byteData, cache.ExpTime).Err(); err != nil {
				//logging the error and returning the error as well
				log.Errorf("Errorf while setting value in redis for - [Key - %v] [Error - %v]", cache.AllRoleKey, err)
			}
			return allRoles, err
		}
	} else {
		allRoles := []models.Roles{}
		log.Infof("Found data in Redis for - [Key - %v]", cache.AllRoleKey)
		if err := json.Unmarshal([]byte(val), &allRoles); err != nil {
			return nil, err
		}
		return allRoles, nil
	}
}
