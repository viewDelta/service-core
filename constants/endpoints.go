package constants

//Endpoints for the application
const (
	//Auth Service Endpoints
	Login           = "/login"
	SignUp          = "/signup"
	VerifyAccount   = "/verify"
	RestorePassword = "/reset"
	FetchNonAdmin   = "/getUsers"
	UserDirective   = "/user/:uid"
	UpdateUsers     = "/users"

	//Mail Service Endpoints
	SendMail = "/sendMail"

	// Products Service EndPoint
	Distro   = "/distributor"
	Category = "/category"
	Type     = "/type"
	Products = "/products"

	//UI Endpoints
	IndexPage           = "/"
	ErrorPage           = "/error"
	ResetEmailEnterPage = "/resetPwd"
	HomePage            = "/home"
	ProfilePage         = "/profile"
	ResetPage           = "/pwdRestore"
)
