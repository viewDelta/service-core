package constants

const (
	// API responses
	INVALID_HEADER                      string = "missing required headers"
	INVALID_TOKEN                       string = "invalid token provided"
	INVALID_CREDENTIALS                 string = "invalid email or password"
	INVALID_PAYLOAD                     string = "invalid payload received"
	INVALID_PROUDCT_CATEGORY            string = "invalid product category"
	EMPTY_PROUDCT_CATEGORY            	string = "empty product category received"
	REQUEST_PROCESSING_ERROR            string = "request processing error"
	REQUEST_PROCESSED_SUCCESS           string = "request processed successfully"
	DATA_NOT_FOUND                      string = "requested data not found"
	UNAUTHORISED_ACCESS                 string = "permission denied"
	MISSING_TOKEN                       string = "token expected but found null"
	TOKEN_GENERATION_ERROR              string = "token generation error"
	LOGGED_IN_SUCESS                    string = "logged in successfully"
	APPLICATION_ERROR                   string = "server unable to respond"
	INVALID_EMAIL                       string = "invalid email"
	EMAIL_IS_REQUIRED                   string = "email is required"
	INVALID_NAME                        string = "invalid first or last name"
	INVALID_PASSWORD                    string = "password must be 6 char long"
	EMAIL_ALREADY_EXISTS                string = "email already exists"
	SIGNUP_SUCESS                       string = "account has been created"
	SIGNUP_SUCESS_WITHOUT_VERIFY_MAIL   string = "account has been created but unable to send signup mail"
	PASSWORD_HASH_ERROR                 string = "password encryption error"
	RECORD_FETCH_ERROR                  string = "record fetch error"
	RECORD_NOT_FOUND_ERROR              string = "record not found"
	RECORD_UPDATATION_ERROR             string = "record updation failed"
	RECORD_UPDATE_SUCCESS               string = "record updated successfully"
	USER_ACCOUNT_NOT_ACTIVE_OR_VERIFIED string = "user account isn't active or verified yet"
	USER_ACTIVATED                      string = "user account activated"
	PASSWORD_UPDATE_SUCCESS             string = "password update successfully"
	ALREADY_VERIFIED                    string = "account already verified"
	RECORD_FETCHED_SUCCESS              string = "record fetched successfully"
	INVALID_ROLEID                      string = "invalid roleId provided"
	ROLEID_NOT_EXISTS                   string = "roleID doesn't exists"
	ROLEID_REQUIRED                     string = "roleID is required"
	NO_CHANGE_REQUESTED                 string = "no change requested"
	INVALID_ENDPOINT                    string = "invalid endpoint"
	INVALID_OPERATION                   string = "invalid operation requested"
	RECORD_DELETED                      string = "record deleted successfully"
	RECORD_PROCESSED_SUCCESS            string = "record processed successfully"
	RECORD_PROCESSED_PARTIALLY          string = "record has been processed partially"
	DETAILS_ALREADY_PRESENT             string = "details already present"

	//Mail API RESPONSE MESSAGES
	EMAIL_CONFIGURATION_NOT_SET     string = "couldn't able to connect host mail DNS"
	PORT_VALUE_EXPECTED             string = "invalid port value provided for mail"
	REQUEST_TYPE_MAIL_NOT_SUPPORTED string = "requested mail type not supported"
	UNABLE_TO_SEND_MAIL             string = "mail request rejected at DNS"
	TEMPLATE_FILE_NOT_FOUND         string = "mail reference file not found"
	TEMPLATE_PARSING_ERROR          string = "template file couldn't be formed"
	MAIL_SENT_SUCCESS               string = "mail sent successfully"
	SIGNUP_MAIL_ALREADY_REQUESTED   string = "signup mail already requested"
	MAIL_ENDPOINT_URL_EXPECTED      string = "mail endpoint url expected"

	//Header Response
	INVALID_X_TYPE      string = "invalid x type requested"
	INVALID_CLIENT_CODE string = "invalid client code"
	INVALID_PRODUCT_MAPPING string = "invalid or missing Product-Mapping received"

	//Casbin Errors
	CASBIN_CONF_FILE_NOT_FOUND string = "casbin conf file not found"

	//Redis Error Message
	CONNECTION_ERROR string = "connection couldn't established"

	// Validation Error Messages
	INVALID_COUNTRY_CODE = "invalid country code"
)
