package proxy

const (
	SEND_MAIL_ENDPOINT      string = "/sendMail"
	VERIFY_ACCOUNT_ENDPOINT string = "/verify"
)
