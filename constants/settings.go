package constants

const (
	CLIENT       string = "retailGO"
	AdminID      string = "1"

	// User password hash count
	PasswordHash int = 2
	// token renew time limit in hour
	TokenRenewTime int = 1
	// Gzip compression level
	GzipLevel = 5
	//network Settings Name
	DEFAULT_HOST      string = "localhost"
	COOKIE_TOKEN_NAME string = "session"
)
