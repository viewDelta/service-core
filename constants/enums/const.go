package enums

const (
	SIGNUP = "SignUp"
	RESET  = "Reset"

	// ---------- ContactInfo request type ----------
	REPLACE string = "R"
	UPDATE  string = "U"

	// ---------- WorkFlow Status ------------------
	CREATED              string = "CREATED"
	UPDATED              string = "UPDATED"
	DELETED              string = "DELETED"
	APPROVED             string = "APPROVED"
	REJECTED             string = "REJECTED"
	WAITING_FOR_APPROVAL string = "WAITING FOR APPROVAL"
)
