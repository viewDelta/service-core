package constants

const (
	USERS            string = "users"
	ROLES            string = "roles"
	MailErrorHistory string = "mail-error-history"
	DISTRIBUTOR      string = "distributor"
	PRODUCT_MAPPING  string = "products-mapping"
	PRODUCT_CATEGORY string = "products-category"
	PRODUCT          string = "products"
)
