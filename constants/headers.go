package constants

const (
	HEADER_NickName  = "nickName"
	HEADER_Email     = "email"
	HEADER_FirstName = "firstName"
	HEADER_LastName  = "lastName"
	
	//CORS HEADERS
	TOKEN_HEADER     string = "Token"
	X_Forwarded_Host string = "X-Forwarded-Host"

	// Get Product-Category Headers
	HEADER_CATEGORY = "category"
	HEADER_PRODUCT_MAPPING = "Product-Mapping"
)
