package constants

//Env NAMES
const (
	DBEndpointEnv    string = "MONGO_URI"
	DBNameEnv        string = "MONGO_DB"
	RedisEndpointEnv string = "REDIS_URI"
	RedisPwdEnv      string = "REDIS_PWD"
	JwtKeyEnv        string = "jwtKey"
)
