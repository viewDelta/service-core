package models

import (
	"fmt"
	"time"

	"bitbucket.org/viewDelta/service-core/constants"
	"github.com/Kamva/mgm/v3"
)

// Distributor will have distributor details
type Distributor struct {
	mgm.DefaultModel `bson:",inline"`
	DeletedAt        time.Time     `json:"deletedAt" bson:"deletedAt"`
	IsDeleted        bool          `json:"isDeleted" bson:"isDeleted"`
	FirstName        string        `json:"firstName" bson:"firstName"`
	LastName         string        `json:"lastName" bson:"lastName"`
	Email            string        `json:"email" bson:"email"`
	Address          string        `json:"address" bson:"address"`
	ContactInfo      []ContactInfo `json:"contactInfo" bson:"contactInfo"`
}

// ContactInfo will have info about contact
type ContactInfo struct {
	CountryCode string `json:"countryCode" bson:"countryCode"`
	ContactNo   string `json:"contactNo" bson:"contactNo"`
}

func (distro *Distributor) String() string {
	return fmt.Sprintf("Distributor Details { Name - %v, EMAIL - %v }", distro.FirstName+" "+distro.LastName, distro.Email)
}

// CollectionName will return collection name
func (distro *Distributor) CollectionName() string {
	return constants.DISTRIBUTOR
}
