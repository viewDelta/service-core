package models

import (
	"bitbucket.org/viewDelta/service-core/constants"
	"github.com/Kamva/mgm/v3"
)

//Roles model for mongoDB
type Roles struct {
	mgm.DefaultModel `bson:",inline"`
	RoleID           string `json:"roleID" bson:"roleID"`
	RoleName         string `json:"roleName" bson:"roleName"`
}

//CollectionName will return collection name
func (model *Roles) CollectionName() string {
	return constants.ROLES
}
