package models

import (
	"fmt"
	"time"

	"bitbucket.org/viewDelta/service-core/constants"
	"github.com/Kamva/mgm/v3"
)

// Products is the model for products collection
type Products struct {
	mgm.DefaultModel     `bson:",inline"`
	IsDeleted            bool      `json:"isDeleted" bson:"isDeleted"`
	ProductID            string    `json:"productId" bson:"productId"`
	ProductType          string    `json:"type" bson:"type"`
	CategoryID           string    `json:"CategoryID" bson:"CategoryID"`
	ProductName          string    `json:"productName" bson:"productName"`
	ProductDes           string    `json:"productDesc" bson:"productDesc"`
	manufacturingDetails `json:"manufacturingDetails" bson:"manufacturingDetails"`
	workFlowStatus `json:"workFlowStatus" bson:"workFlowStatus"`
}

type manufacturingDetails struct {
	Manufacturer      string    `json:"manufacturer" bson:"manufacturer"`
	MadeIn            string    `json:"madeIn" bson:"madeIn"`
	ManufacturingTime time.Time `json:"manufactureTime" bson:"manufactureDate"`
	ExpTime           time.Time `json:"expTime" bson:"expTime"`
	IsLiquid          bool      `json:"isLiquid" bson:"isLiquid"`
	PackageContents   int       `json:"packageContents" bson:"packageContents"`
	Dimension         struct {
		Length  float64 `json:"le" bson:"le"`
		Breadth float64 `json:"bt" bson:"bt"`
		Height  float64 `json:"ht" bson:"ht"`
		Weight  float64 `json:"wt" bson:"wt"`
		Unit    struct {
			SizeUnit   string `json:"size" bson:"size"`
			WeightUnit string `json:"wt" bson:"wt"`
		} `json:"unit" bson:"unit"`
	} `json:"dimension" bson:"dimension"`
}

// CollectionName will return collection name
func (p *Products) CollectionName() string {
	return constants.PRODUCT
}

func (p *Products) String() string {
	return fmt.Sprintf("Product Category Details {ProductID - %v, ProductType - %v, CategoryID - %v, ProductName - %v, ManufacturerDetails - %v}", p.ProductID, p.ProductType, p.CategoryID, p.ProductName, p.manufacturingDetails)
}
