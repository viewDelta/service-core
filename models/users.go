package models

import (
	"fmt"
	"time"

	"bitbucket.org/viewDelta/service-core/constants"
	"github.com/Kamva/mgm/v3"
)

//Users to interact with USER object with MongoDB
type Users struct {
	mgm.DefaultModel `bson:",inline"`
	UID              string    `json:"uid" bson:"uid"`
	FirstName        string    `json:"firstName" bson:"firstName"`
	LastName         string    `json:"lastName" bson:"lastName"`
	Email            string    `json:"email" bson:"email"`
	Password         string    `json:"password" bson:"password"`
	Active           bool      `json:"active" bson:"active"`
	Verified         bool      `json:"verified" bson:"verified"`
	IsDeleted        bool      `json:"isDeleted" bson:"isDeleted"`
	DeletedAt        time.Time `json:"deletedAt" bson:"deletedAt"`
	Role             []string  `json:"role" bson:"role"`
	Session          struct {
		Id    string    `json:"id",bson:"id"`
		ReqAt time.Time `json:"reqAt" bson:"reqAt"`
		ExpAt time.Time `json:"expAt" bson:"expAt"`
	} `json:"session" bson:"session"`
	VerifyRequest struct {
		Id        string    `json:"id",bson:"id"`
		ReqAt     time.Time `json:"reqAt" bson:"reqAt"`
		EnabledAt time.Time `json:"enabledAt" bson:"enabledAt"`
		ExpAt     time.Time `json:"expAt" bson:"expAt"`
	} `json:"verifyRequest" bson:"verifyRequest"`
	ResetRequest struct {
		Id    string    `json:"id",bson:"id"`
		ReqAt time.Time `json:"reqAt" bson:"reqAt"`
		ExpAt time.Time `json:"expAt" bson:"expAt"`
	} `json:"resetRequest" bson:"resetRequest"`
}

func (model *Users) String() string {
	return fmt.Sprintf("User Details [EMAIL - %v, UID - %v]", model.Email, model.UID)
}

//CollectionName will return collection name
func (model *Users) CollectionName() string {
	return constants.USERS
}
