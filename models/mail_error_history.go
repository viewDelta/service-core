package models

import (
	"fmt"

	"bitbucket.org/viewDelta/service-core/constants"
	"github.com/Kamva/mgm/v3"
)

//MailErrorHistory to keep failed email error list
type MailErrorHistory struct {
	mgm.DefaultModel `bson:",inline"`
	Email            string `json:"email" bson:"email"`
	XType            string `json:"x-type" bson:"x-type"`
	Attempts         int    `json:"attempts" bson:"attempts"`
	IsSuccess        bool   `json:"isSuccess" bson:"isSuccess"`
}

//NewMailErrorHistory will return instance reference of mail_error_history
func NewMailErrorHistory(email, xtype string) *MailErrorHistory {
	return &MailErrorHistory{
		Email:     email,
		XType:     xtype,
		Attempts:  1,     //Default value to 1, as object creation states that, email is not been sent and tried once
		IsSuccess: false, //Default value to false, as object creation states that, email is not been sent
	}
}

func (model *MailErrorHistory) String() string {
	return fmt.Sprintf("Mail-Error-Details [EMAIL - %v, X-Type - %v, Attempts - %v, IsSuccess - %v]", model.Email, model.XType, model.Attempts, model.IsSuccess)
}

//CollectionName will return collection name
func (model *MailErrorHistory) CollectionName() string {
	return constants.MailErrorHistory
}
