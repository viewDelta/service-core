package models

import (
	"bitbucket.org/viewDelta/service-core/constants"
	"fmt"
	"github.com/Kamva/mgm/v3"
)

// ProductMapping is model for product-mapping collection
type ProductMapping struct {
	mgm.DefaultModel `bson:",inline"`
	IsDeleted        bool             `json:"isDeleted" bson:"isDeleted"`
	Type             string           `json:"type" bson:"type"`
	Categories       []string         `json:"categories" bson:"categories"`
	WorkFlowStatus   []workFlowStatus `json:"workFlowStatus" bson:"workFlowStatus"`
}

func (pm *ProductMapping) String() string {
	return fmt.Sprintf("ProductMapping Details {Name - %v}", pm.Type)
}

// CollectionName will return collection name
func (pm *ProductMapping) CollectionName() string {
	return constants.PRODUCT_MAPPING
}
