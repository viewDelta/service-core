package models

import (
	"fmt"
	"time"

	"bitbucket.org/viewDelta/service-core/constants"
	"github.com/Kamva/mgm/v3"
)

// ProductCategory for category for product
type ProductCategory struct {
	mgm.DefaultModel `bson:",inline"`
	IsDeleted        bool             `json:"isDeleted" bson:"isDeleted"`
	CategoryName     string           `json:"categoryName" bson:"categoryName"`
	DistributorIDs   []string         `json:"distributorIDs" bson:"distributorIDs"`
	WorkFlowStatus   []workFlowStatus `json:"workFlowStatus" bson:"workFlowStatus"`
}

type workFlowStatus struct {
	RequestedBy string    `json:"requestedBy" bson:"requestedBy"`
	TimeStamp   time.Time `json:"timeStamp" bson:"timeStamp"`
	Comment     string    `json:"comment" bson:"comment"`
}

// NewWorkFlowStatus will return new workFlowStatus reference object
func NewWorkFlowStatus(userEmail, comment string) *workFlowStatus {
	return &workFlowStatus{
		RequestedBy: userEmail,
		TimeStamp:   time.Now(),
		Comment:     comment,
	}
}

func (pc *ProductCategory) String() string {
	return fmt.Sprintf("Product Category Details {Name - %v, WorkFlowStatus - %+v}", pc.CategoryName, pc.WorkFlowStatus)
}

// CollectionName will return collection name
func (pc *ProductCategory) CollectionName() string {
	return constants.PRODUCT_CATEGORY
}
