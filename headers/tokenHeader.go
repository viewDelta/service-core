package headers

import (
	"fmt"

	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/models"
	"bitbucket.org/viewDelta/service-core/repository"
	"bitbucket.org/viewDelta/service-core/utils"
	"github.com/labstack/gommon/log"
)

//TokenHeader is used to call api based on only token data
type TokenHeader struct {
	Token    string `header:"Token"`
	category string
}

const (
	forgotPwdOrSession string = "FORGOT_PASSWORD_OR_SESSION"
	verifyAccount      string = "VERIFY_ACCOUNT"
	session            string = "SESSION"
)

//SetForgotPwdOrSession will set category object as forgotPassword or session
func (t *TokenHeader) SetForgotPwdOrSession() {
	t.category = forgotPwdOrSession
}

//SetVerifyAccount will set category object as verifyAccount
func (t *TokenHeader) SetVerifyAccount() {
	t.category = verifyAccount
}

//SetSession will set category object as session
func (t *TokenHeader) SetSession() {
	t.category = session
}

//ValidateToken will return respective user for the associated token if iser is found in DB
func (t *TokenHeader) ValidateToken(userDAO repository.UserDAO) (*models.Users, error) {
	if t.Token == "" {
		log.Errorf("Empty Token received")
		return nil, fmt.Errorf(constants.INVALID_TOKEN)
	}
	if clamis, err := utils.GetTokenDetails(t.Token); err != nil {
		log.Errorf("Token Parsing Error [%v]", err)
		return nil, fmt.Errorf(constants.INVALID_TOKEN)
	} else {
		user, err := userDAO.FindUser(clamis.Email)
		if err != nil {
			return nil, err
		}
		switch t.category {
		case forgotPwdOrSession:
			if user.ResetRequest.Id != t.Token && user.Session.Id != t.Token {
				return nil, fmt.Errorf(constants.INVALID_TOKEN)
			}
		case verifyAccount:
			if user.VerifyRequest.Id != t.Token {
				return nil, fmt.Errorf(constants.INVALID_TOKEN)
			} else if !user.VerifyRequest.EnabledAt.IsZero() {
				log.Errorf("User already verified and request can't be processed")
				return nil, fmt.Errorf(constants.ALREADY_VERIFIED)
			}
		case session:
			if user.Session.Id != t.Token {
				return nil, fmt.Errorf(constants.INVALID_TOKEN)
			}
		default:
			return nil, fmt.Errorf(constants.INVALID_TOKEN)
		}
		//Token Type Validation whether same exists in db or not
		return user, nil
	}
}
