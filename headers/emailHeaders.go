package headers

import (
	"fmt"

	"bitbucket.org/viewDelta/service-core/constants/enums"
	"bitbucket.org/viewDelta/service-core/utils"

	"bitbucket.org/viewDelta/service-core/constants"
)

//EmailHeader will be header validator for the email request
type EmailHeader struct {
	Client string `header:"Client"`
	XType  string `header:"X-Type"`
}

const (
	//SignUpHeaderValue value will trigger signup mail
	signUpHeaderValue = enums.SIGNUP
	//ResetHeaderValue will trigger reset password mail
	resetHeaderValue = enums.RESET

	//HEADER FOR REQUESTS
	XTYPE  = "X-TYPE"
	CLIENT = "Client"
)

var (
	xTypes []string
)

func init() {
	xTypes = append(xTypes, signUpHeaderValue, resetHeaderValue)
}

//ValidateHeader will validate email RequestHeader
func (e *EmailHeader) ValidateHeader() error {
	if !utils.Contains(xTypes, e.XType) {
		return fmt.Errorf(constants.INVALID_X_TYPE)
	}
	return nil
}

func (e *EmailHeader) String() string {
	return fmt.Sprintf("Requested Identifier Client-[%v], Type-[%v]", e.Client, e.XType)
}

//IsSignUpReq will return
func (e *EmailHeader) IsSignUpReq() bool {
	return e.XType == signUpHeaderValue
}

//GetSignUpHeader will return email service required header
func GetSignUpHeader() *EmailHeader {
	var e EmailHeader
	e.XType = signUpHeaderValue
	e.Client = constants.CLIENT
	return &e
}

//GetResetHeader will return email service required header
func GetResetHeader() *EmailHeader {
	var e EmailHeader
	e.XType = resetHeaderValue
	e.Client = constants.CLIENT
	return &e
}

//GetAllXTypes will return all X-Type for mail Header
func GetAllXTypes() []string {
	return xTypes
}
