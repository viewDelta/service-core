package outbound

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/viewDelta/service-core/config"
	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/constants/proxy"
	"bitbucket.org/viewDelta/service-core/headers"
	"bitbucket.org/viewDelta/service-core/locales/request"
	"github.com/labstack/gommon/log"

	"bitbucket.org/viewDelta/service-core/locales"
)

var (
	emailBody   request.MailReq
	apiResponse locales.ResponseTemplate
)

//MailCall will call for mail micro-service
func MailCall(header *headers.EmailHeader, email, host string) error {
	log.Info("<-- Inside of MailCall Methods -->")
	if host == "" {
		return fmt.Errorf(constants.MAIL_ENDPOINT_URL_EXPECTED)
	}
	host = config.GetConfiguration().Network.Protocol + host + proxy.SEND_MAIL_ENDPOINT
	log.Infof("Outgoing Request For - [%v]", host)
	//Request body formation
	emailBody.Email = email
	reqBodyBytes := new(bytes.Buffer)
	if err := json.NewEncoder(reqBodyBytes).Encode(&emailBody); err != nil {
		return err
	}
	req, err := http.NewRequest("POST", host, bytes.NewBuffer(reqBodyBytes.Bytes()))
	if err != nil {
		return err
	}
	//Set Headers
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set(headers.XTYPE, header.XType)
	req.Header.Set(headers.CLIENT, constants.CLIENT)

	client := &http.Client{}
	// Send request
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	log.Info(">-- Exiting MailCall Method --<")
	if resp.StatusCode == http.StatusOK {
		return nil
	} else {
		if err := json.NewDecoder(resp.Body).Decode(&apiResponse); err != nil {
			return nil
		}
		return fmt.Errorf(apiResponse.Msg)
	}
}
