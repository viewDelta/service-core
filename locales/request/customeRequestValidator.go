package request

import (
	"bitbucket.org/viewDelta/service-core/constants/enums"
	"bitbucket.org/viewDelta/service-core/utils"
	"github.com/go-playground/validator/v10"
	"strings"
)

var updateType []string

func init() {
	updateType = append(updateType, enums.REPLACE)
	updateType = append(updateType, enums.UPDATE)
}

func IsValidRequestType(reqType validator.FieldLevel) bool {
	return utils.Contains(updateType, strings.TrimSpace(reqType.Field().String()))
}
