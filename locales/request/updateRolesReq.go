package request

//UpdateRolesRequest will update the role information for users
type UpdateRolesRequest struct {
	UID   string   `json:"uid"`
	ROLES []string `json:"roles"`
}
