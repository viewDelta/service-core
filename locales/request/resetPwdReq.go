package request

//ResetPasswordRequest request struct for password
type ResetPasswordRequest struct {
	Password string `json:"password"`
}
