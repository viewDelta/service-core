package request

type DropProductCategoryRequest struct {
	Categories []string `json:"categories"`
}
