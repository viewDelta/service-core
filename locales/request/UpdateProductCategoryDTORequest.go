package request

import (
	"fmt"
)

type UpdateProductCategoryDTORequest struct {
	CategoryName   string   `json:"categoryName" validate:"required,alpha"`
	Type           string   `json:"type" validate:"required,len=1,alpha,IsValidRequestType"`
	DistributorIDs []string `json:"distributorIDs" validate:"required,dive,alphanum"`
}

func (c *UpdateProductCategoryDTORequest) String() string {
	return fmt.Sprintf("CategoryName Received - {Name : %v, Type : %v} ", c.CategoryName, c.Type)
}
