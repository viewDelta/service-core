package request

import (
	"encoding/json"
	"fmt"
	"strings"
	
	"bitbucket.org/viewDelta/service-core/utils"
	"bitbucket.org/viewDelta/service-core/models"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/gommon/log"
)

// CreateDistroDTOReq request type for creation of distributor
type CreateDistroDTOReq struct {
	FirstName   string        `json:"firstName" validate:"required,alpha"`
	LastName    string        `json:"lastName" validate:"required,alpha"`
	Email       string        `json:"email" validate:"required,email"`
	Address     string        `json:"address" validate:"required"`
	ContactInfo []contactInfo `json:"contactInfo" validate:"required,dive"`
}

type contactInfo struct {
	CountryCode string `json:"countryCode" validate:"omitempty,len=2,alpha"`
	ContactNo   string `json:"contactNo" validate:"omitempty,min=3,max=13,number"`
}

// Validate will validate given payload is valid or not
func (c *CreateDistroDTOReq) Validate(validate *validator.Validate, allDistros []models.Distributor) error {
	log.Info("Entering CreateDistroDTOReq.Validate() function")
	log.Infof("Request struct - %v", c.String())
	if err := validate.Struct(c); err != nil {
		log.Errorf("Validation Error - %v",err)
		return utils.ConvertValidationErrToString(err)
	}
	for _, contact := range c.ContactInfo {
		if utils.IsBlankString(contact.ContactNo) || utils.IsBlankString(contact.CountryCode){
			log.Error("Empty Contact number or country Code received")
			return fmt.Errorf("expected countryCode and contactNumber but found empty")
		}
		if err := utils.IsValidAlpha2CountryCode(contact.CountryCode);err!=nil{
			return err
		}
		if checkForNewNumber(allDistros,contact.CountryCode,contact.ContactNo){
			log.Errorf("Country Code or Phone Number is already present in DB")
			return fmt.Errorf("number is already present in db")
		}
	}
	log.Info("Exiting CreateDistroDTOReq.Validate() function")
	return nil
}

func (c *CreateDistroDTOReq) String() string {
	info, _ := json.Marshal(c)
	return fmt.Sprintf("%v", string(info))
}

// checkForNewNumber will check if the number is totally new
// returns true, if number is already present in db 
// false, if record is not found in db
func checkForNewNumber(allDistros []models.Distributor, countryCode,phNumber string) bool{
	for _,eachDistro := range allDistros{
		for _,contact := range eachDistro.ContactInfo{	
			if strings.EqualFold(contact.ContactNo,phNumber) && strings.EqualFold(contact.CountryCode,countryCode){
				return true
			}
		}
	}
	return false
}