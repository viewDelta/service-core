package request

//LoginReq will be the login request sent by the user
type LoginReq struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	Persist  bool   `json:"persist"`
}
