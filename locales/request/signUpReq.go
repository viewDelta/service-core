package request

//SignUpReq request body for signning up
type SignUpReq struct {
	FirstName string   `json:"firstName"`
	LastName  string   `json:"lastName"`
	Email     string   `json:"email"`
	Password  string   `json:"password"`
	RoleID    []string `json:"roleId"`
}
