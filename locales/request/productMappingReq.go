package request

import (
	"fmt"
)

// ProductMappingRequest will be payload for create/update product-mapping request
type ProductMappingRequest struct {
	Type        string   `json:"type" validate:"required,alpha"`
	Categories []string `json:"categories" validate:"required,dive,alpha"`
}

func (object *ProductMappingRequest) String() string {
	return fmt.Sprintf("CategoryName Received - {type : %v, categories : %+q}", object.Type, object.Categories)
}
