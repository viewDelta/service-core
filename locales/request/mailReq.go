package request

//MailReq will be used for email Request Body
type MailReq struct {
	Email string `json:"email"`
}
