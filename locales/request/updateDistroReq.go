package request

import (
	"encoding/hex"
	"encoding/json"
	"fmt"

	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/models"
	"bitbucket.org/viewDelta/service-core/utils"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/gommon/log"
)

// UpdateDistroReq will update data in db
type UpdateDistroReq struct {
	ID                string        `json:"_id" validate:"required,hexadecimal"`
	Email             string        `json:"email" validate:"omitempty,email"`
	Address           string        `json:"address" validate:"omitempty,min=2,alphanum"`
	ContactUpdateType string        `json:"type" validate:"omitempty,len=1,alpha,IsValidRequestType"`
	ContactInfo       []contactInfo `json:"contactInfo" validate:"omitempty,dive"`
}

// Validate will validate given payload is valid or not
func (c *UpdateDistroReq) Validate(validate *validator.Validate, allDistros []models.Distributor) error {
	log.Info("Entering UpdateDistroReq.Validate() function")
	log.Infof("Request struct - %v", c.String())
	if err:=  validate.Struct(c);err!=nil{
		log.Errorf("Validation Error - %v",err)
		return utils.ConvertValidationErrToString(err)
	}
	if utils.IsBlankString(c.Email) && utils.IsBlankString(c.Address) && utils.IsBlankString(c.ContactUpdateType) && nil == c.ContactInfo {
		return fmt.Errorf(constants.NO_CHANGE_REQUESTED)
	}
	if _, err := hex.DecodeString(c.ID); err != nil {
		return fmt.Errorf("Invalid id received - %v", c.ID)
	}
	for _, contact := range c.ContactInfo {
		if err := utils.IsValidAlpha2CountryCode(contact.CountryCode);err!=nil{
			return err
		}
		if checkForNewNumber(allDistros, contact.CountryCode,contact.ContactNo){
			log.Errorf("Country Code or Phone Number is already present in DB")
			return fmt.Errorf("number is already present in db")
		}
	}
	log.Info("Exiting UpdateDistroReq.Validate() function")
	return nil
}

func (c *UpdateDistroReq) String() string {
	info, _ := json.Marshal(c)
	return fmt.Sprintf("%v", string(info))
}
