package request

import (
	"github.com/go-playground/validator/v10"
	"github.com/labstack/gommon/log"
	"bitbucket.org/viewDelta/service-core/utils"
)

// DeleteOnIDReq to delete collection from DB
type DeleteOnIDReq struct {
	ID string `json:"id" validate:"required,hexadecimal"`
}

// Validate will validate whether it's hexCode id or not
func (obj *DeleteOnIDReq) Validate(validate *validator.Validate) error {
	log.Info("Entering DeleteOnIDReq.Validate() function")
	log.Infof("Request struct - {'id'- '%v'}", obj.ID)
	if err:=  validate.Struct(obj);err!=nil{
		log.Errorf("Validation Error - %v",err)
		return utils.ConvertValidationErrToString(err)
	}
	log.Info("Exiting DeleteOnIDReq.Validate() function")	
	return nil
}
