package request

//ActivateReq for user enable or disable request
type ActivateReq struct {
	Flag bool `json:"activate"`
}
