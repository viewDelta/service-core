package request

import (
	"fmt"
)

type CreateProductCategoryDTORequest struct{
	CategoryName     string    `json:"categoryName" validate:"required,alpha"`
	DistributorIDs   []string  `json:"distributorIDs" validate:"required,dive,alphanum"`
}

func (c *CreateProductCategoryDTORequest) String() string{
	return fmt.Sprintf("CategoryName Received - [%v]", c.CategoryName)
}