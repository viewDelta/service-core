package response

import (
	"bitbucket.org/viewDelta/service-core/locales"
	countrycodes "github.com/launchdarkly/go-country-codes"
	"time"
)

// FetchDistroResponse is response for get distro details
type FetchDistroResponse struct {
	locales.ResponseTemplate
	DistroDetails []FetchDistro `json:"distributors"`
}

// FetchDistro is user details
type FetchDistro struct {
	ID               string    `json:"id"`
	CreatedTimeStamp time.Time `json:"created_at"`
	FirstName        string    `json:"firstName"`
	LastName         string    `json:"lastName"`
	Email            string    `json:"email"`
	Address          string    `json:"address"`
	ContactInfo      []struct {
		CountryCode string `json:"countryCode"`
		ContactNo   string `json:"contactNo"` // should have the countryCode with mobile number
	} `json:"contactInfo"`
}

// FormContactAppend will update country dial-in code with phone number
func (obj *FetchDistro) FormContactAppend() {
	for index, contact := range obj.ContactInfo {
		if countryDetails, ok := countrycodes.GetByAlpha2(contact.CountryCode); ok {
			obj.ContactInfo[index].ContactNo = countryDetails.DialingCode + "-" + contact.ContactNo
		}
	}
}
