package response

import (
	"bitbucket.org/viewDelta/service-core/locales"
	"time"
)

type PCategoryDetails struct {
	CategoryName     string        `json:"categoryName"`
	CreatedTimeStamp time.Time     `json:"created_at"`
	Distributors     []FetchDistro `json:"distributors"`
}

// FetchProductCategoryResponse for product-category details
type FetchProductCategoryResponse struct {
	locales.ResponseTemplate
	CategoryDetails []PCategoryDetails `json:"details"`
}