package response

import (
	"time"

	"bitbucket.org/viewDelta/service-core/locales"
)

//LoginRes will be the response sent via server
type LoginRes struct {
	Status locales.ResponseTemplate `json:"status"`
	Data   struct {
		Token     string    `json:"token"`
		CreatedAt time.Time `json:"createdAt"`
		ExpAt     time.Time `json:"expAt"`
	} `json:"data"`
}
