package response

import (
	"bitbucket.org/viewDelta/service-core/locales"
	"bitbucket.org/viewDelta/service-core/models"
	"bitbucket.org/viewDelta/service-core/utils"
	"encoding/json"
	"github.com/labstack/gommon/log"
	"time"
)

// ProductMappingResponse will be response for product-Mapping request
type ProductMappingResponse struct {
	locales.ResponseTemplate
	Data []data `json:"data"`
}

type data struct {
	CreatedAt  time.Time `json:"created_at"`
	Type       string    `json:"type"`
	Categories []string  `json:"categories"`
}

func (obj *ProductMappingResponse) String() string {
	jsonInfo, err := json.Marshal(obj)
	if err!=nil{
		log.Errorf("Error while marshalling error - %v",err)
		return ""
	}
	return string(jsonInfo)
}

func (obj *ProductMappingResponse) AppendData(productMappingFromDB *models.ProductMapping) error{
	var newData data
	if err:=utils.UseObjectMapper(productMappingFromDB, &newData);err!=nil{
		return err
	}
	obj.Data = append(obj.Data, newData)
	return nil
}