package response

import "bitbucket.org/viewDelta/service-core/locales"

// UpdateResponse will return list of success or errorList contents
type UpdateResponse struct {
	locales.ResponseTemplate
	ErrorList []Details `json:"errorList"`
}

// Details for more information in update code
type Details struct {
	ErrorCode    int    `json:"errorCode"`
	ErrorMessage string `json:"info"`
}

// NewErrorDetail will return new instance of Details
func NewErrorDetail(errorCode int, info string) Details {
	return Details{
		ErrorCode:    errorCode,
		ErrorMessage: info,
	}
}

// FormUpdateResponse will form the object for api response
func FormUpdateResponse(code int, msg string, details []Details) *UpdateResponse {
	var newUpdateResponse UpdateResponse
	newUpdateResponse.ResponseTemplate.Code = code
	newUpdateResponse.ResponseTemplate.Msg = msg
	newUpdateResponse.ErrorList = details
	return &newUpdateResponse
}
