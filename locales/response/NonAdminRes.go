package response

import (
	"bitbucket.org/viewDelta/service-core/locales"
)

//NonAdminListResponse will be struct response for the getAllNonAdmin users
type NonAdminListResponse struct {
	locales.ResponseTemplate
	UserDetailsList []UserDetails `json:"userDetailsList"`
}

//UserDetails for indivisual user response data
type UserDetails struct {
	ID        string `json:"uid"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Activated bool   `json:"activated"`
	Verified  bool   `json:"verified"`
}
