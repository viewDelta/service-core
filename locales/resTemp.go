package locales

//ResponseTemplate for response struct
type ResponseTemplate struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}
