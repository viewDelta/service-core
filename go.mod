module bitbucket.org/viewDelta/service-core

go 1.14

require (
	github.com/Kamva/mgm/v3 v3.0.1
	github.com/casbin/casbin/v2 v2.2.3
	github.com/casbin/mongodb-adapter/v2 v2.1.0
	github.com/dennisstritzke/httpheader v0.0.0-20191001125049-c4997e500c10
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/validator/v10 v10.3.0
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/graphql-go/graphql v0.7.9
	github.com/graphql-go/handler v0.2.3
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.1.16
	github.com/labstack/gommon v0.3.0
	github.com/launchdarkly/go-country-codes v0.0.0-20191008001159-776cf5214f39
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/tchap/go-patricia v2.3.0+incompatible // indirect
	go.mongodb.org/mongo-driver v1.3.4
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/yaml.v2 v2.2.8
)
