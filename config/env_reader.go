package config

import (
	"os"

	"github.com/labstack/gommon/log"
)

//GetEnv will read the env properties defined,
//if property is absent it'll returns empty string
func GetEnv(env string) string {
	log.Infof("Looking for Env - || %v ||", env)
	value, ok := os.LookupEnv(env)
	if ok != true {
		log.Warnf("Unable To Locate Env [>> %v <<]", env)
	}
	return value
}
