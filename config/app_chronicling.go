package config

import (
	"io"
	"os"
	"time"

	"github.com/labstack/gommon/log"
	"gopkg.in/yaml.v2"
)

type retryJob struct {
	Enable      bool     `yaml:"enable"`
	CronExp     string   `yaml:"cronExp"`
	MaxAttempts int      `yaml:"maxAttempts"`
	Qualifier   []string `yaml:"qualifier"`
}

//AppConfiguration struct loaded from config.yml
type AppConfiguration struct {
	Application struct {
		Name    string `yaml:"name"`
		Port    string `yaml:"port"`
		Version string `yaml:"version"`
		LogDir  string `yaml:"logDir"`
	} `yaml:"application"`
	Network struct {
		Protocol string `yaml:"protocol"`
	} `yaml:"network"`
	Validation struct {
		EmailRegEx string `yaml:"emailRegEx"`
		NameRegEx  string `yaml:"nameRegEx"`
	} `yaml:"validation"`
	Redis struct {
		Database int `yaml:"database"`
		ExpTime  int `yaml:"expTime"`
	} `yaml:"redis"`
	RetryJob retryJob `yaml:"retryJob"`
}

const (
	configFileName = "config.yml"
)

var (
	//CFG will load the yml information
	cfg AppConfiguration
	//File Reference to log file
	file *os.File

	ticker = time.NewTicker(24 * time.Hour)
	done   = make(chan bool, 1)
)

func init() {
	var (
		configPath string //config.yml file path with relative to current directory
	)
	if dir, err := os.Getwd(); err == nil {
		configPath = dir + "/" + configFileName
	} else {
		panic("Unable to find executable directory\n" + err.Error())
	}
	if file, err := os.Open(configPath); err != nil {
		panic("Unable to open config file\n" + err.Error())
	} else {
		if err := yaml.NewDecoder(file).Decode(&cfg); err != nil {
			panic(configFileName + " File issue \n" + err.Error())
		}
	}
	//Checking the log directory otherwise create directory
	if _, err := os.Stat(cfg.Application.LogDir); os.IsNotExist(err) {
		if err := os.MkdirAll(cfg.Application.LogDir, 0777); err != nil {
			panic(err)
		}
	}
	createLogFile()
	go logFileCheck()
}

func createLogFile() {
	file, err = os.OpenFile(cfg.Application.LogDir+cfg.Application.Name+time.Now().Format("2006-01-02")+".log", os.O_CREATE|os.O_APPEND|os.O_WRONLY|os.O_SYNC, 0644)
	if err != nil {
		panic(err)
	}
	//Log write into both console & file - For Local Testing
	writer := io.MultiWriter(os.Stdout, file)
	log.SetOutput(writer)
	log.SetHeader("${time_rfc3339} ${level} ${prefix} ${short_file}(${line})${prefix}") //Formating of logs
	log.Infof("Loaded The Properties for %v", cfg.Application.Name)
}

//CloseLogFile will close the log file reference. Need to call this method
// at the end of server termination.
func CloseLogFile() {
	done <- true
}

//tearDown will shut down the log file connection
func tearDown() {
	log.Info("Closing logfile Connection")
	if err := file.Close(); err != nil {
		log.Errorf("Error While Closing the log file [%v]", err)
	}
}

//logFileCheck go routine will generate log file from the 24-Hours gap from the time of start execution of code
func logFileCheck() {
	for {
		select {
		case <-done:
			tearDown()
			return
		case <-ticker.C:
			tearDown()
			createLogFile()
		}
	}
}

//GetConfiguration - Getting Configuration Object
func GetConfiguration() *AppConfiguration {
	return &cfg
}
