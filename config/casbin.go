package config

import (
	"github.com/casbin/casbin/v2"
	"github.com/casbin/casbin/v2/model"
	mongodbadapter "github.com/casbin/mongodb-adapter/v2"
	"github.com/labstack/gommon/log"
)

var (
	enforcer *casbin.Enforcer
	err      error
)

//GetCasbinEnforcer returns casbin enforcer instance
func GetCasbinEnforcer() (*casbin.Enforcer, error) {
	if enforcer == nil {
		log.Info("[ Loading Casbin Web Security Module ]")
		//Calling this, to load the db connection string from env
		loadDbProp()
		policyModel, err := model.NewModelFromString(casbinConf)
		if err != nil {
			return nil, err
		}
		adaptper, err := mongodbadapter.NewAdapter(dbConnURL)
		if err != nil {
			return nil, err
		}
		enforcer, err = casbin.NewEnforcer(policyModel, adaptper)
		if err != nil {
			return nil, err
		}
		// Load the policy from DB.
		if err := enforcer.LoadPolicy(); err != nil {
			return nil, err
		}
		log.Info("[ Loaded Casbin Web Security Module ]")
	}
	return enforcer, nil
}
