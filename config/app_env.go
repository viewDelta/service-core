package config

import (
	"bitbucket.org/viewDelta/service-core/constants"
	"github.com/joho/godotenv"
	"github.com/labstack/gommon/log"
)

//Env Values
var (
	// dbConnURL need to have the default casbin collection database for authentication
	dbConnURL, redisURI, redisPwd string
)

func init() {
	log.Info("Looking For .env File")
	err := godotenv.Load()
	if err != nil {
		log.Warnf("No .env file was found. Switching to default env lookup")
		return
	}
	log.Info(".env File was found and loaded")
}

//loadDbProp will load database configuration
func loadDbProp() {
	//Checking if the properties are empty then read from environment variables
	if dbConnURL == ""  {
		dbConnURL = GetEnv(constants.DBEndpointEnv)
	}
}

//loadRedisProp will load redis configuration
func loadRedisProp() {
	//Checking if the properties are empty then read from environment variables
	if redisURI == "" || redisPwd == "" {
		redisURI = GetEnv(constants.RedisEndpointEnv)
		redisPwd = GetEnv(constants.RedisPwdEnv)
	}
}
