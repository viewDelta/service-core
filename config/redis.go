package config

import (
	"fmt"

	"bitbucket.org/viewDelta/service-core/constants"
	"github.com/go-redis/redis"
	"github.com/labstack/gommon/log"
)

var client *redis.Client

//GetRedisClient will return redis configured client
func GetRedisClient() (*redis.Client, error) {
	if client == nil {
		//Loading redis properties if it's not loaded
		loadRedisProp()
		client = redis.NewClient(&redis.Options{
			Addr:     redisURI,
			Password: redisPwd,
			DB:       GetConfiguration().Redis.Database, // use default DB
		})
		if _, err := client.Ping().Result(); err != nil {
			return nil, fmt.Errorf(constants.CONNECTION_ERROR)
		}
	}
	return client, nil
}

//CloseRedisClient will close the redis connection
func CloseRedisClient() error {
	if client != nil {
		log.Error("---->>> Closing Redis Connection <<<----")
		return client.Close()
	}
	return nil
}
