package config

import (
	"github.com/Kamva/mgm/v3"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"strings"
	"time"
)

//SetupDB will connection to database
func SetupDB(reqDB string) {
	loadDbProp()
	if len(strings.TrimSpace(reqDB)) > 0 {
		if err := mgm.SetDefaultConfig(nil, strings.TrimSpace(reqDB), options.Client().ApplyURI(dbConnURL));
			err != nil {
			log.Fatalf("Error occured while connecting to DB [%v]", err)
			return
		}
		_, client, _, _ := mgm.DefaultConfigs()
		if err := client.Ping(mgm.NewCtx(10*time.Second), readpref.Primary()); err != nil {
			log.Fatalf("Error In DB Connection [%v]", err)
		}
		return
	}
	log.Error("!!! Expected Database Name for connection !!!")
}
