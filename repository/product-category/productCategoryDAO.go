package productcategory

import (
	"bitbucket.org/viewDelta/service-core/config"
	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/models"
	"bitbucket.org/viewDelta/service-core/repository"
	"fmt"
	"github.com/Kamva/mgm/v3"
	"github.com/labstack/gommon/log"
	"gopkg.in/mgo.v2/bson"
)

type productCategoryImpl struct {
	collection *mgm.Collection
}

var (
	productCategoryRepo *productCategoryImpl
)

// New to get new ProductCategory repo object
func New() repository.IProductCategoryDAO {
	if productCategoryRepo == nil {
		//setup mgm connection
		config.SetupDB(constants.DatabaseProducts)
		productCategoryRepo = &productCategoryImpl{
			collection: mgm.Coll(&models.ProductCategory{}),
		}
	}
	return productCategoryRepo
}

func (repo *productCategoryImpl) Insert(newPCategory *models.ProductCategory) error{
	log.Infof("Inserting Record as, %v", newPCategory)
	return repo.collection.Create(newPCategory)
}
func (repo *productCategoryImpl)  Update(oldPCategory *models.ProductCategory) error{
	return repo.collection.Update(oldPCategory)
}
func (repo *productCategoryImpl)  FindAll() ([]models.ProductCategory, error){
	pTypeList := []models.ProductCategory{}
	if err := repo.collection.SimpleFind(&pTypeList , bson.M{
		"isDeleted": false,
	}); err != nil {
		return nil, err
	}
	return pTypeList , nil
}

func (repo *productCategoryImpl)  FindByID(id string) (*models.ProductCategory, error){
	pType := &models.ProductCategory{}
	if err := repo.collection.FindByID(id, pType); err != nil {
		return nil, err
	}
	if pType.IsDeleted {
		return nil, fmt.Errorf(constants.RECORD_NOT_FOUND_ERROR)
	}
	return pType, nil
}

func (repo *productCategoryImpl)  FindByCategory(category string) (*models.ProductCategory, error){
	pType := &models.ProductCategory{}
	if err := repo.collection.First(bson.M{
		"isDeleted": false,
		"categoryName":category,
	}, pType); err != nil {
		return nil, err
	}
	return pType, nil
}