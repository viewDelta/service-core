package mailerrorhistory

import (
	"bitbucket.org/viewDelta/service-core/config"
	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/models"
	"bitbucket.org/viewDelta/service-core/repository"
	"github.com/Kamva/mgm/v3"
	"gopkg.in/mgo.v2/bson"
)

type mailErrorHistoryImpl struct {
	collection *mgm.Collection
}

var (
	mailErrorRepo *mailErrorHistoryImpl
)

//New to get new USER repo object
func New() repository.MailErrorHistoryDAO {
	if mailErrorRepo == nil {
		//setup mgm connection
		config.SetupDB(constants.DatabaseRetailGO)
		mailErrorRepo = &mailErrorHistoryImpl{
			collection: mgm.Coll(&models.MailErrorHistory{}),
		}
	}
	return mailErrorRepo
}

func (repo *mailErrorHistoryImpl) FindAllNotSent(maxAttempts int) ([]models.MailErrorHistory, error) {
	errorList := []models.MailErrorHistory{}
	if err := repo.collection.SimpleFind(&errorList, bson.M{
		"attempts": bson.M{
			"$lt": maxAttempts,
		},
		"isSuccess": false,
	}); err != nil {
		return nil, err
	}
	return errorList, nil
}

func (repo *mailErrorHistoryImpl) FindAllOnlyOnXType(xtype string, maxAttempts int) ([]models.MailErrorHistory, error) {
	errorList := []models.MailErrorHistory{}
	if err := repo.collection.SimpleFind(&errorList, bson.M{
		"attempts": bson.M{
			"$lt": maxAttempts,
		},
		"isSuccess": false,
		"x-type":    xtype,
	}); err != nil {
		return nil, err
	}
	return errorList, nil
}

func (repo *mailErrorHistoryImpl) InsertMailError(mailError *models.MailErrorHistory) error {
	return (repo.collection.Create(mailError))
}

func (repo *mailErrorHistoryImpl) UpdateMailError(mailError *models.MailErrorHistory) error {
	return (repo.collection.Update(mailError))
}

func (repo *mailErrorHistoryImpl) FindAnyExistingError(email, xtype string) (*models.MailErrorHistory, error) {
	mailError := &models.MailErrorHistory{}
	if err := repo.collection.First(bson.M{
		"email":     email,
		"isSuccess": false,
		"x-type":    xtype,
	}, mailError); err != nil {
		return nil, err
	}
	return mailError, nil
}
