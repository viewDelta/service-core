package repository

import "bitbucket.org/viewDelta/service-core/models"

//UserDAO layer for accessing the user db methods
type UserDAO interface {
	FindUser(Email string) (*models.Users, error)
	FindUserByID(UID string) (*models.Users, error)
	InsertUser(user *models.Users) error
	UpdateUser(user *models.Users) error
	DropUser(Email string) error
	FetchAllNonAdmin() ([]models.Users, error)
}

//RoleDAO layer for accessing role DB methods
type RoleDAO interface {
	FindRole(roleID string) (*models.Roles, error)
	GetAll() ([]models.Roles, error)
}

//MailErrorHistoryDAO for accessing mail_error_history DB methods
type MailErrorHistoryDAO interface {
	FindAllNotSent(maxAttempts int) ([]models.MailErrorHistory, error)
	FindAllOnlyOnXType(xtype string, maxAttempts int) ([]models.MailErrorHistory, error)
	InsertMailError(*models.MailErrorHistory) error
	UpdateMailError(*models.MailErrorHistory) error
	FindAnyExistingError(email, xtype string) (*models.MailErrorHistory, error)
}

// IDistributorDAO distributor interface
type IDistributorDAO interface {
	Insert(*models.Distributor) error
	Update(*models.Distributor) error
	FindAll() ([]models.Distributor, error)
	FindByEmail(email string) (*models.Distributor, error)
	FindByFirstName(firstName string) ([]models.Distributor, error)
	FindByLastName(lastName string) ([]models.Distributor, error)
	FindByID(id string) (*models.Distributor, error)
	DeleteByID(id string) error
}

// IProductTypeDAO distributor interface
type IProductCategoryDAO interface {
	Insert(*models.ProductCategory) error
	Update(*models.ProductCategory) error
	FindAll() ([]models.ProductCategory, error)
	FindByID(string) (*models.ProductCategory, error)
	FindByCategory(string) (*models.ProductCategory, error)
}

type IProductMappingDAO interface {
	Insert (*models.ProductMapping) error
	Update(*models.ProductMapping) error
	FindAll() ([]models.ProductMapping, error)
	FindByMapping(string) (*models.ProductMapping, error)
}