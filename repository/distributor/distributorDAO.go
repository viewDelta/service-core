package distributor

import (
	"fmt"
	"time"

	"bitbucket.org/viewDelta/service-core/config"
	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/models"
	"bitbucket.org/viewDelta/service-core/repository"
	"github.com/Kamva/mgm/v3"
	"gopkg.in/mgo.v2/bson"
)

type distributorImpl struct {
	collection *mgm.Collection
}

var (
	distributorRepo *distributorImpl
)

// New to get new Distributor repo object
func New() repository.IDistributorDAO {
	if distributorRepo == nil {
		//setup mgm connection
		config.SetupDB(constants.DatabaseProducts)
		distributorRepo = &distributorImpl{
			collection: mgm.Coll(&models.Distributor{}),
		}
	}
	return distributorRepo
}

// Insert will insert new record into the database
func (repo *distributorImpl) Insert(newDistro *models.Distributor) error {
	return repo.collection.Create(newDistro)
}

// Update will insert new update into the database
func (repo *distributorImpl) Update(distro *models.Distributor) error {
	return repo.collection.Update(distro)
}

// FindAll will fetch all the non-deleted user
func (repo *distributorImpl) FindAll() ([]models.Distributor, error) {
	distroList := []models.Distributor{}
	if err := repo.collection.SimpleFind(&distroList, bson.M{
		"isDeleted": false,
	}); err != nil {
		return nil, err
	}
	return distroList, nil
}

// FindByEmail find distro by nickName
func (repo *distributorImpl) FindByEmail(email string) (*models.Distributor, error) {
	distro := &models.Distributor{}
	if err := repo.collection.First(bson.M{"email": email, "isDeleted": false}, distro); err != nil {
		return nil, err
	}
	return distro, nil
}

// FindByFirstName will fetch all records based on matching firstName
func (repo *distributorImpl) FindByFirstName(firstName string) ([]models.Distributor, error) {
	distroList := []models.Distributor{}
	if err := repo.collection.SimpleFind(&distroList, bson.M{"firstName": firstName, "isDeleted": false}); err != nil {
		return nil, err
	}
	return distroList, nil
}

// FindByLastName will fetch all records based on matching firstName
func (repo *distributorImpl) FindByLastName(lastName string) ([]models.Distributor, error) {
	distroList := []models.Distributor{}
	if err := repo.collection.SimpleFind(&distroList, bson.M{"lastName": lastName, "isDeleted": false}); err != nil {
		return nil, err
	}
	return distroList, nil
}

// FindByID will retrive distro record based _id field of document
func (repo *distributorImpl) FindByID(id string) (*models.Distributor, error) {
	distro := &models.Distributor{}
	if err := repo.collection.FindByID(id, distro); err != nil {
		return nil, err
	}
	if distro.IsDeleted {
		return nil, fmt.Errorf(constants.RECORD_NOT_FOUND_ERROR)
	}
	return distro, nil
}

// DeleteByID will delete record based on _id
func (repo *distributorImpl) DeleteByID(id string) error {
	distro, err := repo.FindByID(id)
	if err != nil {
		return err
	}
	distro.IsDeleted = true
	distro.DeletedAt = time.Now()
	return repo.collection.Update(distro)
}
