package product_mapping

import (
	"bitbucket.org/viewDelta/service-core/config"
	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/models"
	"bitbucket.org/viewDelta/service-core/repository"
	"github.com/Kamva/mgm/v3"
	"github.com/labstack/gommon/log"
	"gopkg.in/mgo.v2/bson"
)

type productMappingImpl struct {
	collection *mgm.Collection
}

var (
	productMappingRepo *productMappingImpl
)

func New() repository.IProductMappingDAO {
	if productMappingRepo == nil {
		//setup mgm connection
		config.SetupDB(constants.DatabaseProducts)
		productMappingRepo = &productMappingImpl{
			collection: mgm.Coll(&models.ProductMapping{}),
		}
	}
	return productMappingRepo
}

func (repo *productMappingImpl) Insert(newPMapping *models.ProductMapping) error {
	log.Infof("Inserting Record as, %v to collection - %v", newPMapping.String(), newPMapping.CollectionName())
	return repo.collection.Create(newPMapping)
}
func (repo *productMappingImpl) Update(oldPMapping *models.ProductMapping) error {
	log.Infof("Updating Record as, %v to collection - %v", oldPMapping.String(), oldPMapping.CollectionName())
	return repo.collection.Update(oldPMapping)
}
func (repo *productMappingImpl) FindAll() ([]models.ProductMapping, error) {
	log.Infof("Fetching All Record from collection - %v", constants.PRODUCT_MAPPING)
	pCategoryList := []models.ProductMapping{}
	if err := repo.collection.SimpleFind(&pCategoryList, bson.M{
		"isDeleted": false,
	}); err != nil {
		return nil, err
	}
	return pCategoryList, nil
}

func (repo *productMappingImpl) FindByMapping(mapping string) (*models.ProductMapping, error) {
	log.Infof("Fetching Mapping Record for - {%v} from collection - %v", mapping, constants.PRODUCT_MAPPING)
	pType := &models.ProductMapping{}
	if err := repo.collection.First(bson.M{
		"isDeleted": false,
		"type":      mapping,
	}, pType); err != nil {
		return nil, err
	}
	return pType, nil
}
