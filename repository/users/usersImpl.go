package users

import (
	"time"

	"bitbucket.org/viewDelta/service-core/config"
	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/models"
	"bitbucket.org/viewDelta/service-core/repository"
	"github.com/Kamva/mgm/v3"
	"gopkg.in/mgo.v2/bson"
)

type userImpl struct {
	collection *mgm.Collection
}

var (
	userRepo *userImpl
)

//New to get new USER repo object
func New() repository.UserDAO {
	if userRepo == nil {
		//setup mgm connection
		config.SetupDB(constants.DatabaseRetailGO)
		userRepo = &userImpl{
			collection: mgm.Coll(&models.Users{}),
		}
	}
	return userRepo
}

//FindUser will fetch email related information from the DB
func (repo *userImpl) FindUser(Email string) (*models.Users, error) {
	user := &models.Users{}
	if err := repo.collection.First(bson.M{"email": Email, "isDeleted": false}, user); err != nil {
		return nil, err
	}
	return user, nil
}

func (repo *userImpl) FindUserByID(UID string) (*models.Users, error) {
	user := &models.Users{}
	if err := repo.collection.First(bson.M{"uid": UID, "isDeleted": false}, user); err != nil {
		return nil, err
	}
	return user, nil
}

//InsertUser will insert record into database
func (repo *userImpl) InsertUser(user *models.Users) error {
	return repo.collection.Create(user)
}

//UpdatetUser will update the record into database
//Before calling update make sure to have fetch operation first as, _id field will be idenetifier for the transaction
func (repo *userImpl) UpdateUser(user *models.Users) error {
	return repo.collection.Update(user)
}

//DropUser will remove the entry of the matching record
func (repo *userImpl) DropUser(Email string) error {
	userData, err := repo.FindUser(Email)
	if err != nil {
		return err
	}
	userData.IsDeleted = true
	userData.DeletedAt = time.Now()
	return repo.collection.Update(userData)
}

//FetchAllNonAdmin will return all non-admin users
func (repo *userImpl) FetchAllNonAdmin() ([]models.Users, error) {
	userList := []models.Users{}
	if err := repo.collection.SimpleFind(&userList, bson.M{
		"role":      bson.M{"$ne": constants.AdminID},
		"isDeleted": false,
	}); err != nil {
		return nil, err
	}
	return userList, nil
}
