package roles

import (
	"bitbucket.org/viewDelta/service-core/config"
	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/models"
	"bitbucket.org/viewDelta/service-core/repository"
	"github.com/Kamva/mgm/v3"
	"gopkg.in/mgo.v2/bson"
)

type roleImpl struct {
	collection *mgm.Collection
}

var (
	roleRepo *roleImpl
)

//New to get new ROLE repo object
func New() repository.RoleDAO {
	if roleRepo == nil {
		//setup mgm connection
		config.SetupDB(constants.DatabaseRetailGO)
		roleRepo = &roleImpl{
			collection: mgm.Coll(&models.Roles{}),
		}
	}
	return roleRepo
}

func (repo *roleImpl) FindRole(roleID string) (*models.Roles, error) {
	role := &models.Roles{}
	if err := repo.collection.First(bson.M{"roleID": roleID}, role); err != nil {
		return nil, err
	}
	return role, nil
}

func (repo *roleImpl) GetAll() ([]models.Roles, error) {
	allRoles := []models.Roles{}
	if cursor, err := repo.collection.Find(mgm.Ctx(), bson.M{}); err != nil {
		return nil, err
	} else if err := cursor.All(mgm.Ctx(), &allRoles); err != nil {
		return nil, err
	}
	return allRoles, nil
}
