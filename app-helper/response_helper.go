package helper

import (
	"net/http"

	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/locales"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
)

//SendGenericResponse will send the response template echo context response
func SendGenericResponse(c echo.Context, errorCode int, errorMsg string) error {
	var res locales.ResponseTemplate
	res.Code = errorCode
	res.Msg = errorMsg
	return c.JSON(errorCode, &res)
}

// ErrorHandler will return error response
func ErrorHandler(c echo.Context) error {
	log.Warnf("Application Error")
	return SendGenericResponse(c, http.StatusInternalServerError, constants.APPLICATION_ERROR)
}

// SendSuccessResponse return ok response
func SendSuccessResponse(c echo.Context) error {
	return SendGenericResponse(c, http.StatusOK, constants.REQUEST_PROCESSED_SUCCESS)
}

// GetSuccessObject will return json struct object for api
func GetSuccessObject() interface{} {
	var res locales.ResponseTemplate
	res.Code = http.StatusOK
	res.Msg = constants.REQUEST_PROCESSED_SUCCESS
	return &res
}

// GetSuccessObject will return custom json struct object for api
func CustomResponseObject(errorCode int, errorMsg string) interface{} {
	var res locales.ResponseTemplate
	res.Code = errorCode
	res.Msg = errorMsg
	return &res
}

func ErrorLogger(c echo.Context, err error) error {
	log.Errorf("Error while fetching record - %v", err)
	return ErrorHandler(c)
}
